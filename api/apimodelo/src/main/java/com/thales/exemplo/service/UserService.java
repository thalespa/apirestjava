package com.thales.exemplo.service;

import java.util.List;

import org.jvnet.hk2.annotations.Service;

import com.thales.exemplo.entidades.User;


public interface UserService {
	
	List<User> listarTodos();
	User listarPorId(Integer id);
	User cadastrar(User user);
	User atualizar(User user);
	void remover(Integer id);

	

}
