package com.thales.exemplo.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.thales.exemplo.entidades.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	
	
}
