package com.thales.exemplo.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.thales.exemplo.entidades.EmailUser;

public interface EmailUserRepositorio extends CrudRepository<EmailUser, Integer> {

}
