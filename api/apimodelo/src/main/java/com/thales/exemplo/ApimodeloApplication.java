package com.thales.exemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApimodeloApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApimodeloApplication.class, args);
	}

}
