package com.thales.exemplo.entidades;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

@Entity 
@Table(name = "user_tbl")
public class User {
	
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
	    private Integer user_id;
	    private String nome;
	    private String cpf;
	    
	    @OneToMany(cascade = CascadeType.ALL,targetEntity=EmailUser.class)
	    private Collection<EmailUser> email;
	    
		@OneToOne(cascade = CascadeType.ALL,targetEntity=EnderecoUser.class)
	    private EnderecoUser endereco;
	    
	    
	    public Integer getUser_id() {
			return user_id;
		}


		public void setUser_id(Integer user_id) {
			this.user_id = user_id;
		}

		public Collection<EmailUser> getEmail() {
			return email;
		}


		public void Collection(List<EmailUser> email) {
			this.email = email;
		}


		public EnderecoUser getEndereco() {
			return endereco;
		}


		public void setEndereco(EnderecoUser endereco) {
			this.endereco = endereco;
		}

			public Integer getId() {
			return user_id;
		}


		public void setId(Integer user_id) {
			this.user_id = user_id;
		}

		@NotEmpty(message = "Nome não pode ser vazio")
		//@Min(value = 3, message ="Tamanho do nome menor que o permitido")
		//@Max(value = 100, message = "Tamanho do nome maior que o permitido")
		public String getNome() {
			return nome;
		}


		public void setNome(String nome) {
			this.nome = nome;
		}


		@NotEmpty(message = "Cpf não pode ser vazio")
		@CPF(message = "CPF inválido")
		public String getCpf() {
			return cpf;
		}
	
		public void setCpf(String cpf) {
			this.cpf = cpf;
		}

		public User() {
	    	
	    }

}
