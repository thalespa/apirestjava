package com.thales.exemplo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.thales.exemplo.entidades.Login;
import com.thales.exemplo.entidades.Roler;
import com.thales.exemplo.response.Response;

@Transactional
@RestController
@CrossOrigin
@RequestMapping(path = "/api/role")
public class GetUserPermisson {


	@CrossOrigin
	@PostMapping
	public ResponseEntity<Response<Roler>> getRoler(@RequestBody Login login, BindingResult result) {
	
		Roler roler = new Roler();
		roler.setChave("0000-1111-4444-555-6666-11");
	
		if(login.getLogin().equals("admin") && login.getSenha().equals("123456")) {
			
			roler.setRoler("admin");
			roler.setAdmin(true);
		}
			
		else if(login.getLogin().equals("comum")&& login.getSenha().equals("123456")) {
			
			roler.setAdmin(false);
			roler.setRoler("comum");
			
		}
		else {
			roler.setAdmin(false);
			roler.setChave("error");
			roler.setRoler("error");
		}
			
		
		return ResponseEntity.ok(new Response<Roler>(roler));
	}
}
