package com.thales.exemplo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thales.exemplo.entidades.User;
import com.thales.exemplo.response.Response;
import com.thales.exemplo.service.UserService;

@Transactional
@RestController
@RequestMapping(path = "/api/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	
	@GetMapping
	public ResponseEntity<Response<List<User>>> listarTodos(){
	
		return ResponseEntity.ok(new Response<List<User>>(this.userService.listarTodos()));
	}
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<Response<User>> listarPorid(@PathVariable(name = "id") Integer id ){
		
		return ResponseEntity.ok(new Response<User>(this.userService.listarPorId(id)));
		
	}
	
	@PostMapping
	public ResponseEntity<Response<User>> cadastrar(@Valid @RequestBody User user, BindingResult result){
		
		if(result.hasErrors()) {
		
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return ResponseEntity.badRequest().body(new Response<User>(erros));
			
		}
		//this.enderecoServer.cadastrar(user.getEndereco());
		return ResponseEntity.ok(new Response<User>(this.userService.cadastrar(user)));
	}
	
	@PutMapping(path = "/{id}")
	public ResponseEntity<Response<User>> atualizar(@PathVariable(name = "id") Integer id, @Valid @RequestBody User user, BindingResult result){
	
		if(result.hasErrors()) {
		
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return ResponseEntity.badRequest().body(new Response<User>(erros));
			
		}
		user.setId(id);
		return ResponseEntity.ok(new Response<User>(this.userService.cadastrar(user)));
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Response<Boolean>> remover(@PathVariable(name = "id") Integer id){
		
		this.userService.remover(id);
		return ResponseEntity.ok(new Response<Boolean>(true));
	}
	
}
