class infraDados {

    static setRoler = (roler) => {
 
         localStorage.setItem("roler", roler);
      
    }
    static setToken = (token) => {
    
        localStorage.setItem("token", token);
       
    }
    
    static getRoler = () => {
     
        var roler = localStorage.getItem("roler");
    
        return roler;
      
    }
    static getToken = () => {
    
        var token = localStorage.getItem("token");
    
        return token;
       
    }

}
export default infraDados;

