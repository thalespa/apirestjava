import React from 'react'
import infraDados from "../infraDados";
import axios from 'axios';

const baseUrl = 'http://localhost:8080/api/role';

export default class Login extends React.Component{

    state = {
      login: '',
      senha: '',
      isLoaded : false,
      error: '',
      isErro: false,
    }

    componentWillMount() {

    
    }

    updatefieldLogin(event) {
       
      var login = { ...this.state.login }
      login = event.target.value /**em target pegamos o conteúdo de input name */
      this.setState({ login})
    }
    updatefieldSenha(event) {
       
      var senha = { ...this.state.senha }
      senha = event.target.value /**em target pegamos o conteúdo de input name */
      this.setState({ senha})
    }

       constuctor() {

        this.routeChange = this.routeChange.bind(this);

      }
    

      routeChange() {

        var login = {};
        login.login = this.state.login;
        login.senha = this.state.senha;
        var send = {
          method: "post",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(login)
        };

        this.setState({
          isErro: false,
          error: ''
        });

        fetch(baseUrl, send)
        .then(response => response.json())
        .then(
          (response) => {

            console.log(response);
            console.log(response.data.chave);
            if(response.data.chave != "error"){

              infraDados.setRoler(response.data.admin);
              infraDados.setToken(response.data.chave);
         
              let path = `users`;
              this.props.history.push(path);
            }
            else {
              this.setState({
                isErro: true,
                error: "Login e senha inválidos"
              });
             
      
            }
              

          },
          (error) => {
            console.log("ERROR API");
            console.log(error);
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
     
      }

    render(){        


        return(<div className="form">
                     <br></br>
                     <br></br>
                     <br></br>
                    <div className="row">
                      <div className="col-md-4"></div>
                        <div className="col-md-3">
                            <div className="form-group">
                                <label htmlFor="login">Login</label>
                                <input type="text" className="form-control" 
                                    name="login" 
                                    value={this.state.login}
                                    onChange={e => this.updatefieldLogin(e)}
                                    placeholder=""
                                    />
                            </div>
                            <div className="form-group">
                                <label htmlFor="senha">Senha</label>
                                <input type="password" className="form-control" 
                                    name="senha" 
                                    value={this.state.senha}
                                    onChange={e => this.updatefieldSenha(e)}
                                    />
                            </div>
                            <button type="button"  onClick={() => this.routeChange()} class="btn btn-primary btn-lg">Entrar
                            </button>
                            <label>{this.state.error}</label>
                        </div>
                        <div className="col-md-4"></div>
                    </div>

                    
                    
                

                </div>
           );
    }
}