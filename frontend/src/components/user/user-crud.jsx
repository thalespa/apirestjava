import React from 'react'
import Main from '../template/main'
import fetch from 'cross-fetch'
import Alert from 'react-bootstrap/Alert'
import Logo from '../template/logo'
import Nav from '../template/nav'
import infraDados from "../infraDados";
import InputMask from 'react-input-mask';




const headerProps = {
    icon: 'users',
    title: 'usuários',
    subtitle: 'Cadastro de usuário'
}



const baseUrl = 'http://localhost:8080/api/user'
const initState= {
    
    endereco: {logradouro: '', bairro: '', cidade: '', uf:'', cep: '', numero: '', complemento: ''},
    user: {user_id:'', nome:'', cpf: ''},
    email: '',
    list: [],
    isLoaded: false,
    sucesso: false,
    roler: infraDados.getRoler(),
    error: "",

}



export default class UserCrud extends React.Component{

    state = { ...initState }

    
     atualiza(){

        this.setState({
            isLoaded: false,
            error: ""
          });

        fetch(baseUrl)
        .then(response => response.json())
        .then(
          (response) => {
            console.log(response);
            this.setState({ list: response.data });
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            console.log("ERROR API");
            console.log(error);
            this.setState({
              isLoaded: true,
              error: error
            });
          }
        )
        
    }

    /**Chamada quando o elemento for exibido na tela */
    componentWillMount() {

       this.atualiza();
        
    }

    buscarEndereco(){

        var cep = this.state.endereco["cep"].replace("-", "");
        console.log(cep);

        fetch("http://viacep.com.br/ws/"+cep+"/json/")
        .then(response => response.json())
        .then(
          (response) => {

            var _endereco = {};
            _endereco.logradouro = response.logradouro;
            _endereco.cep = response.cep;
            _endereco.bairro  = response.bairro;
            _endereco.cidade = response.localidade;
            _endereco.uf  = response.uf;
            _endereco.numero  = "";
            _endereco.complemento   = "";
            this.setState({endereco: _endereco})
            console.log(response);
            
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            console.log("ERROR API");
            console.log(error);
            this.setState({
              isLoaded: true,
              error
            });
          }
        )

    }


    /*Limpar formulario */
    clear() {
        this.setState({ user: initState.user, endereco: initState.endereco, email: initState.email })
        window.scrollTo(0,0);
    }


    save() {

        var email = []
        var email_value = {};
        email_value.email =  this.state.email;
        email.push(email_value);

        var user = this.state.user;
        user["cpf"] =  user["cpf"].replace(".", "").replace("-", "").replace(".", "").replace(".", "");
        user.endereco = this.state.endereco;
        user.endereco["cep"] =  user.endereco["cep"].replace("-", "");
        user.email = email;

        console.log(user);

        const method = user.user_id ? 'put' : 'post'
        console.log(method);
        console.log(baseUrl + '/' + user.user_id);

        var send = {
            method: method,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
          };

        fetch(baseUrl + '/' + user.user_id, send)
        .then(response => response.json())
        .then(
          (response) => {

            if(response.erros != null){

                var e = "";
                response.erros.map((err,index) => {

                    e += err + " ";

                });

                this.setState({
                    isLoaded: true,
                    error: e
                  });

                
            }else {


            console.log(response);
            this.setState({ user: initState.user })  
            window.scrollTo(0,0);
            this.atualiza();
            }



            //this.setState({ list: response.data });
          },
          (error) => {
            console.log("ERROR API");
            console.log(error);
            this.setState({
              isLoaded: true,
              error: error
            });
          }
        )
      

    }

    
    updatefieldEmail(event){

        var email = { ...this.state.email }
        email = event.target.value /**em target pegamos o conteúdo de input name */
        this.setState({ email})
    }

    updatefield(event) {
       
        const user = { ...this.state.user }
        user[event.target.name] = event.target.value /**em target pegamos o conteúdo de input name */
        this.setState({ user})
    }
    updatefieldEndereco(event) {
       
        const endereco = { ...this.state.endereco }
        endereco[event.target.name] = event.target.value /**em target pegamos o conteúdo de input name */
        this.setState({ endereco})
    }
    renderForm(){
        /**jsx que ira renderizar o formulário */
        return (
            <div className="form">
               <label >{this.state.error}</label>
                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label htmlFor="name">Nome</label>
                            <input type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="nome" 
                                value={this.state.user.nome}
                                onChange={e => this.updatefield(e)}
                                placeholder="Digite o nome.."
                                />
                        </div>
                        <div className="form-group">
                            <label htmlFor="cpf">CPF</label>
                            <InputMask mask="999.999.999-99" type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="cpf" 
                                value={this.state.user.cpf}
                                onChange={e => this.updatefield(e)}
                                placeholder="digite o cpf.."
                                />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="email" 
                                value={this.state.email}
                                onChange={e => this.updatefieldEmail(e)}
                                placeholder="digite o email.."
                                />
                        </div>
                        <div className="form-group">
                            <label htmlFor="cep">CEP</label>
                            <InputMask mask="99999-999" type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="cep" 
                                value={this.state.endereco.cep}
                                onChange={e => this.updatefieldEndereco(e)}
                                placeholder="digite o cep.."
                                />
                             <button className="btn btn-primary" onClick={e => this.buscarEndereco(e)}>Buscar Endereço</button>
                        </div>
                       
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label htmlFor="logradouro">Endereço</label> 
                            <input type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="logradouro" 
                                value={this.state.endereco.logradouro}
                                onChange={e => this.updatefieldEndereco(e)}
                                placeholder="digite o endereco.."
                                />
                        </div>
                        <div className="form-group">
                            <label htmlFor="bairro">bairro</label>
                            <input type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="bairro" 
                                value={this.state.endereco.bairro}
                                onChange={e => this.updatefieldEndereco(e)}
                                placeholder="digite o bairro.."
                                />
                        </div>
                        <div className="form-group">
                            <label htmlFor="cidade">cidade</label>
                            <input type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="cidade" 
                                value={this.state.endereco.cidade}
                                onChange={e => this.updatefieldEndereco(e)}
                                placeholder="digite o cidade.."
                                />
                        </div>
                        <div className="form-group">
                            <label htmlFor="complemento">complemento</label> 
                            <input type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="complemento" 
                                value={this.state.endereco.complemento}
                                onChange={e => this.updatefieldEndereco(e)}
                                placeholder="digite o complemento.."
                                />
                        </div>
                        <div className="form-group">
                            <label htmlFor="numero">numero</label> 
                            <input type="text" className="form-control" disabled={infraDados.getRoler()  === "false"}
                                name="numero" 
                                value={this.state.endereco.numero}
                                onChange={e => this.updatefieldEndereco(e)}
                                placeholder="digite o numero.."
                                />
                        </div>
                        
                    </div>
                </div>

                <hr />

                <div className="row">
                    <div className="col-12 d-flex justify-content end">
                        <button className="btn btn-primary" disabled={infraDados.getRoler()  === "false"}
                        onClick={e => this.save(e)}>Salvar</button>
                        <button className="btn btn-secondary ml-2"
                        onClick={e => this.clear(e)}>Cancelar</button>
                    </div>
                </div>

            </div>
        );
    }


    /**edição */
    load(user, endereco, email){
        this.setState({ user, endereco, email })/**atualiza o estado da aplicação. */
       
        window.scrollTo(0,0);
        
    }
    remove(user){
      
        console.log(user);
        var send = {
            method: "delete",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
          };

        fetch(baseUrl + '/' + user.user_id, send)
        .then(response => response.json())
        .then(
          (response) => {
            console.log(response);
            this.setState({ user: initState.user})  
            window.scrollTo(0,0);
            this.atualiza();
            //this.setState({ list: response.data });
          },
          (error) => {
            console.log("ERROR API");
            console.log(error);
            this.setState({
              isLoaded: true,
              error
            });
          }
        )

    }

    /**list users */
    rendertable(){
        
        return(
            <table className="table mt-4">
               <thead>
                    <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderows()}
                </tbody>            
            </table>
        );
    }
    renderows(){

       

        if(infraDados.getRoler() === "true"){

            /**mapeando usuários que estão no estado do objeto */
                return this.state.list.map((user,index) => {
                    return (                
                        <tr key={index}>
                            <td>{user.nome}</td>
                            <td>{user.email[0].email}</td>
                            <td>
                                <button className="btn btn-warning mr-2"
                                onClick={() => this.load(user, user.endereco, user.email[0].email)}>
                                    <i className="fa fa-pencil"></i>
                                </button>
                                <button className="btn btn-danger"
                                onClick={() => this.remove(user)}>
                                    <i className="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    );
                })

        }else {

            /**mapeando usuários que estão no estado do objeto */
        return this.state.list.map((user,index) => {

            return (                
                <tr key={index}>
                    <td>{user.nome}</td>
                    <td>{user.email[0].email}</td>
                    <td>
                        <button className="btn btn-primary mr-2"
                        onClick={() => this.load(user, user.endereco, user.email[0].email)}>
                            <i className="fa fa-pencil"></i>
                        </button>
                    </td>
                </tr>
            );
        })

        }

        
    }


    render(){    
       

        return(   
                 <div className="app">
                    <Logo />
                    <Nav />
                    <Main {...headerProps}>
                    
                        
                        {this.renderForm()}
                        {this.rendertable()}
                    
                    </Main>
                    </div>
        );
    }
}