import React from 'react'
import { Switch, Route, Redirect } from 'react-router'

import Home from '../components/home/home'
import UserCrud from '../components/user/user-crud'
import login from '../components/login/login'

/*Mapeamento dos links aos componentes*/
export default props =>
    <Switch>
        <Route exact path="/" component={login} />
        <Route exact path="/login" component={login} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/users" component={UserCrud} />
        <Redirect from="*" to="/login" />
    </Switch>
