import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css';
import './app.css'

import { HashRouter } from 'react-router-dom' 
import Routes from './routes'

export default props =>
    <HashRouter>
        <div>
            <Routes />
        </div>
    </HashRouter>
   